/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
} from 'react-native';

export default class PonyProject extends Component {
    constructor() {
        super();

        this.state = {
            width: 0,
            height: 0
        };
    }

    // Code that apparently will size image to fullscreen?
    componentDidMount() {
        /*Image.getSize(ponyImage.uri, (srcWidth, srcHeight) => {
            const maxHeight = Dimensions.get('window').height; // or something else
            const maxWidth = Dimensions.get('window').width;

            const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
            this.setState({ width: srcWidth * ratio, height: srcHeight * ratio });
        });*/
        //console.log('My width' + this.state.width);
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Welcome to the Pony React Native!
                </Text>
                <Image
                    style={{ width: 200, height: 200, resizeMode: 'contain' }}
                    source={require('./images/cute_pony_drinking_out_of_a_cup.jpg')}
                />
                <Text style={styles.instructions}>
                    To get started, edit index.android.js
                </Text>
                <Text style={styles.instructions}>
                    Double tap R on your keyboard to reload,{'\n'}
                    Shake or press menu button for dev menu
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
    },
    ponyImage: {
        width: '100%',
        height: 'auto',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

AppRegistry.registerComponent('PonyProject', () => PonyProject);
